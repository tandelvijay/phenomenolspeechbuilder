<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PresentationProfile extends Model
{
    protected $fillable = ['client_id', 'user_id', 'presentations_id', 'presentation_profile_data', 'created_at', 'update_at'];
}
