<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_name'
    ];
    

    /**
     * Get the comments for the blog post.
     */
    public function comments()
    {
        return $this->hasMany('App\User');
    }
}
