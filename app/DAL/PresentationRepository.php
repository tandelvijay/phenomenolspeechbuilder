<?php

namespace App\DAL;
use App\Models\Client;
use App\DAL\CommonRepository as common;
use App\Models\Presentation;
use App\User;
use Auth;
use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;

class PresentationRepository extends Repository
{
    private $common;

    /**
     * CONSTRUCTOR
     * @param App $app
     */
    public function __construct(App $app)
    {

        parent::__construct($app);
        $this->common = new common();
    }

    /**
     * @return string
     * to get model for repository use
     */
    function model()
    {
        return 'App\Models\Presentation';
    }


    /**
     * get user's client wise presentation lists
     * @return mixed
     */
    public function getUserClientWisePresentationList()
    {
        $userId = Auth::user()->id;
        try {
            $presentationLists = Presentation::with('client')->where('user_id', $userId)->get();

            $response = array($this->common->success => true);
            $response['data']['records'] = $presentationLists;

        } catch (\Exception $e) {
            $response = $this->common->getErrorMessage($e->getMessage());
        }

        return Response::json($response);
    }

    /**
     * get user's clientId presentation list
     * @return mixed
     */
    public function getClientPresentationList($client_id)
    {
        $userId = Auth::user()->id;
        try {
            $presentationLists = Presentation::with('client')->where('user_id', $userId)->where('client_id', $client_id)->orderBy('created_at', 'DESC')->get();
            $clientDetails = $this->common->getClientDetails($client_id);
            $response = array($this->common->success => true);
            $response['data']['clientDetails'] = $clientDetails;
            $response['data']['records'] = $presentationLists;

        } catch (\Exception $e) {
            $response = $this->common->getErrorMessage($e->getMessage());
        }

        return Response::json($response);
    }


    /**
     * add edit update client
     * @param $data
     * @return mixed
     */
    public function createPresentation($data)
    {
        $userId = Auth::user()->id;

        $validator = $this->validateCreate($data);

        //VALIDATION FUNCTION
        if ($validator->fails()) {
            $response = array($this->common->success => false, 'error' => ['statusCode' => 103, 'message' => 'Validation errors in your request.', 'errorDescription' => $validator->errors()]);

        } else {
            $id = (int)$data['id'];
            $saveData['user_id'] = $userId;
            $saveData['title'] = trim($data['title']);
            $saveData['description'] = trim($data['description']);
            $saveData['client_id'] = trim($data['client_id']);
            $saveData['is_sale_presentation'] = (int)$data['is_sales_presentation'];

            # Presentation details - as json with key value pair
            if(isset($data['presentation_data'])){
                //$saveData['presentation_data'] = trim($data['presentation_data']);

                //SAMPLE PRESENTATION DATA FOR ALL STEPS


                $pData = json_decode('{
  "step_1": [
    {
      "Audience Profile": [
        {
          "key": "Who is the audience?",
          "value": "People in their late 40\'s-60\'s who may have older relatives that they are caring for, or are thinking about their own healthcare needs."
        },
        {
          "key": "How much time do you have, and how interactive? ",
          "value": "15 minutes"
        },
        {
          "key": "What is their current familiarity with the topic?",
          "value": "Intermediate. They know the topic needs to be addressed, but may be afraid to tackle it or unsure of how to go about it."
        },
        {
          "key": "What significant opinions/attitudes are held on this topic?",
          "value": "Fear that the discussion is morbid and or unnecessary \"right now\"."
        }
      ],
      "Action Definition": [
        {
          "key": "Write the SPECIFIC action you want your audience to take. ",
          "value": "Sell copies of the speaker\'s book and collect contact information to follow up"
        },
        {
          "key": "Enter additional comments on the desired action: IE, timing, fallback action, etc.",
          "value": "\"as well as my private consulting assistance for people who need advice as to how to manage a difficult medical situation.\""
        }
      ],
      "Audience Problem": [
        {
          "key": "Broadly summarize the audience\'s problem. ",
          "value": "Elderly family members are stuck in thoughtless healthcare conveyor belts. Patients and families need help to better understand options and processes."
        },
        {
          "key": "Deconstruct it: What are the problem\'s important manifestations / effects? ",
          "value": [
            {
              "key": "Manifestation 1:",
              "value": "Many tests, procedures, and medications actually prove harmful, causing unnecessary suffering and expenses for patients and their loved ones. ",
              "audience understand": "Partially"
            },
            {
              "key": "Manifestation 2:",
              "value": "If families don\'t discuss goals for end-of-life living, elderly patients are--the one whose opinion matters the most---are left without a say.",
              "audience understand": "Partially"
            }
          ]
        }
      ]
    }
  ],
  "step_2": [
    {
      "pyramid": {
        "Insight Layer": [
          {
            "key": "Excessive medical interventions victimize many elderly people in this country.",
            "transition": "…why am I telling you this? Because by having those tough conversations and setting goals for my mom, we avoided the healthcare conveyor belt..."
          },
          {
            "key": "Discussing end of life is a conversation about living, not dying. It’s a gift.",
            "transition": "If you’re wondering why all of this matters, it’s because…"
          },
          {
            "key": "A Power of Attorney May Not Have Power.",
            "transition": "Knowing what questions to ask and defining your goals for healthcare is the only way to ensure that you and your loved ones are getting the best care..."
          }
        ],
        "Data illustration": [
          "25% of all U.S. deaths occur in the LTC",
          "6M seniors will live an average of 2yrs",
          "A POA protects your future self"
        ]
      }
    }
  ],
  "step_4": [
    {
      "key": "Deconstruct and illustrate the problem\'s manifestations",
      "value": [
        {
          "key": "Many tests, procedures, and medications actually prove harmful, causing unnecessary suffering and expenses for patients and their loved ones.",
          "value": "Mr. Johnson story"
        },
        {
          "key": "If families don\'t discuss goals for end-of-life living, elderly patients are--the one whose opinion matters the most---are left without a say.",
          "value": "Mom\'s story"
        },
        {
          "key": "Wills and Powers of Attorneys offer some relief but don\'t catch everything",
          "value": "Phil story"
        }
      ],
      "Solution": "Get the help of an expert sooner rather than later..."
    }
  ]
}');
                $saveData['presentation_data'] = $pData;

            }

            try {
                Db::beginTransaction();

                if ($id) {
                    // update client records
                    $message = 'Presentation updated successfully.';
                    $saveData['updated_at'] = Carbon::now();
                    parent::update($saveData, $id);

                } else {
                    $message = 'Presentation created successfully.';
                    $saveData['created_at'] = Carbon::now();
                    parent::create($saveData);

                }
                DB::commit();
                $response = array($this->common->success => true, 'message' => $message);

            } catch (\Exception $e) {
                DB::rollBack();
                $response = array(
                    $this->common->success => false,
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage()
                    ]
                );
            }
        }

        return Response::json($response);
    }

    /**
     * delete assets
     * @param $id
     * @return mixed
     */
    public function deletePresentation($id)
    {
        try {
            // Presentation::find($id)->delete();
            parent::delete($id);
            //parent::delete($id);
            $response = array($this->common->success => true, 'message' => 'Presentation deleted successfully.');

        } catch (\Exception $e) {
            $response = array(
                $this->common->success => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            );
        }

        return Response::json($response);

    }

    /**
     *  CREATE VALIDATION FUNCTION
     * @param $data
     * @return mixed
     */
    public function validateCreate($data)
    {
        return  Validator::make($data,[
            'title' => "required",
            'description' => "required"
//            'client_id' => "required"
        ],
            [
                'title.required' => 'Title is required.',
                'description.required' => 'Description is required.'
//                'client_id.required' => 'Client is required.'
            ]);
    }
}
