<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PyramidOutcome extends Model
{
    protected $fillable = ['client_id', 'user_id', 'presentations_id', 'key_insight', 'insight', 'created_at', 'update_at'];
}
