<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpeningClosing extends Model
{
    protected $fillable = ['client_id', 'user_id', 'presentations_id', 'open_close', 'created_at', 'update_at'];
}
