<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoryboardSequence extends Model
{
    protected $fillable = ['client_id', 'user_id', 'presentations_id', 'story_board_sequence', 'created_at', 'update_at'];
}
