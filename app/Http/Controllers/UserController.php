<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\User;
use App\UserRole;
use Auth;
use App\DAL\CommonRepository as common;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    private $presentation, $common;
    public function __construct()
    {
        $this->common = new common();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('userProfile')->with(compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $user = Auth::user();

        try {
            $user = User::find($user->id);
            $user->email = !empty($data['email']) ? $data['email'] : $user->email;
            $user->name = !empty($data['name']) ? $data['name'] : $user->name;
            $user->save();
            
            $response = array($this->common->success => true, 'message' => 'User updated successfully.', 'data' => $data);

        } catch (\Exception $e) {
            $response = array(
                $this->common->success => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            );
        }

        return Response::json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        try {
            $user = User::where('id', $id)->first();

            $user->name = $data['name'];
            $user->email = $data['email'];
            if( !empty($data['new_password']) ) {
                $user->password = Hash::make($data['new_password']);
            }
            $user->save();
            
            $users = User::all();
            $response = array($this->common->success => true, 'message' => 'User updated successfully.', 'data' => $users);

        } catch (\Exception $e) {
            $response = array(
                $this->common->success => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            );
        }

        return Response::json($response);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function accountSetting() {
        $user = Auth::user();
        return view('admin.accountSetting')->with(compact('user'));
    }

    public function manageUsers() {
        $users = User::all();
        $user_roles = array();
        foreach(UserRole::all() as $user_role){
            $user_roles[$user_role->id] = $user_role->role_name;
        }
        $user_roles = json_encode($user_roles, false);
        return view('admin.manageUser')->with(compact('users', 'user_roles'));
    }

    public function addUser(Request $request) {
        $data = $request->all();

        try {
            $user = new User;
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->password = Hash::make($data['new_password']);
            $user->user_roles_id = $data['role'];
            $user->save();
            
            $users = User::all();
            $response = array($this->common->success => true, 'message' => 'User updated successfully.', 'data' => $users);

        } catch (\Exception $e) {
            $response = array(
                $this->common->success => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            );
        }

        return Response::json($response);
    }

    public function deleteUser($id) {
        try {
            $user = User::where('id', $id)->first();
            $user->delete();

            $users = User::all();
            $response = array($this->common->success => true, 'message' => 'User deleted successfully.', 'data' => $users);

        } catch (\Exception $e) {
            $response = array(
                $this->common->success => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            );
        }

        return Response::json($response);
    }
}
