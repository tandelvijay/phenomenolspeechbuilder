<?php

namespace App\Http\Controllers;
use App\DAL\PresentationRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\PresentationProfile;
use Auth;
use App\DAL\CommonRepository as common;
use App\PyramidOutcome;
use App\StoryboardSequence;
use App\OpeningClosing;
use Carbon\Carbon;
use App\Models\Client;
use App\Models\Presentation;
use Barryvdh\DomPDF\Facade as PDF;

class PresentationController extends Controller
{

    private $presentation, $common;
    public function __construct(PresentationRepository $presentationRepository)
    {
        $this->presentation = $presentationRepository;
        $this->common = new common();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function exportPdf($id, $presentation_id)
    {
        $user = Auth::user();
        $client = Client::where('id', $id)->first();
        $presentation_profile = PresentationProfile::firstOrCreate(
            ['client_id' =>  $id, 'user_id' => $user->id, 'presentations_id' => $presentation_id]
        );
        $pyramid_outcome = PyramidOutcome::firstOrCreate(
            ['client_id' =>  $id, 'user_id' => $user->id, 'presentations_id' => $presentation_id]
        )->key_insight;
        $story_board = StoryboardSequence::firstOrCreate(
            ['client_id' =>  $id, 'user_id' => $user->id, 'presentations_id' => $presentation_id]
        );
        $opening_closing = OpeningClosing::firstOrCreate(
            ['client_id' =>  $id, 'user_id' => $user->id, 'presentations_id' => $presentation_id]
        );

        $data['id'] = $id;
        $data['presentation_profile'] = $presentation_profile;
        $data['pyramid_outcome'] = $pyramid_outcome;
        $data['story_board'] = $story_board;
        $data['opening_closing'] = $opening_closing;
        $data['user'] = $user;
        $data['client'] = $client;

        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $pdf = PDF::loadView('pdf.export_pdf', $data);
        return $pdf->download('presentation_blueprint.pdf');
    }


    public function getUserPresentations()
    {
        return $this->presentation->getUserClientWisePresentationList();
    }

    public function getClientPresentationsView()
    {
        return view('presentation');
    }

    public function profilePresentationView($id, $presentation_id)
    {
        $presentation = Presentation::find($presentation_id);
        return view('presentationProfile')->with(compact('id', 'presentation'));
    }

    public function pyramidView($id, $presentation_id)
    {
        $userId = Auth::user()->id;
        $presentation_profile = PresentationProfile::firstOrCreate(
            ['client_id' =>  $id, 'user_id' => $userId, 'presentations_id' => $presentation_id]
        )->presentation_profile_data;
        $presentation = Presentation::find($presentation_id);
        return view('pyramid')->with(compact('id', 'presentation_profile', 'presentation'));
    }

    public function storyboardView($id, $presentation_id)
    {
        $userId = Auth::user()->id;
        $pyramid_outcome = PyramidOutcome::firstOrCreate(
            ['client_id' =>  $id, 'user_id' => $userId, 'presentations_id' => $presentation_id]
        )->key_insight;
        $presentation = Presentation::find($presentation_id);
        return view('storyBoard')->with(compact('id', 'pyramid_outcome', 'presentation'));
    }

    public function openingView($id, $presentation_id)
    {
        $userId = Auth::user()->id;
        $presentation_profile = PresentationProfile::firstOrCreate(
            ['client_id' =>  $id, 'user_id' => $userId, 'presentations_id' => $presentation_id]
        )->presentation_profile_data;
        $pyramid_outcome = PyramidOutcome::firstOrCreate(
            ['client_id' =>  $id, 'user_id' => $userId, 'presentations_id' => $presentation_id]
        )->key_insight;
        $story_board = StoryboardSequence::firstOrCreate(
            ['client_id' =>  $id, 'user_id' => $userId, 'presentations_id' => $presentation_id]
        )->story_board_sequence;
        $presentation = Presentation::find($presentation_id);
        return view('opening')->with(compact('id', 'presentation_profile', 'pyramid_outcome', 'story_board', 'presentation'));
    }

    public function blueprintView($id, $presentation_id)
    {
        $user = Auth::user();
        $client = Client::where('id', $id)->first();
        $presentation_profile = PresentationProfile::firstOrCreate(
            ['client_id' =>  $id, 'user_id' => $user->id, 'presentations_id' => $presentation_id]
        );
        $pyramid_outcome = PyramidOutcome::firstOrCreate(
            ['client_id' =>  $id, 'user_id' => $user->id, 'presentations_id' => $presentation_id]
        )->key_insight;
        $story_board = StoryboardSequence::firstOrCreate(
            ['client_id' =>  $id, 'user_id' => $user->id, 'presentations_id' => $presentation_id]
        )->story_board_sequence;
        $opening_closing = OpeningClosing::firstOrCreate(
            ['client_id' =>  $id, 'user_id' => $user->id, 'presentations_id' => $presentation_id]
        )->open_close;
        return view('blueprint')->with(compact(
            'id', 
            'presentation_profile', 
            'pyramid_outcome', 
            'story_board', 
            'opening_closing',
            'user',
            'client'
        ));
    }

    public function getClientPresentations($client_id)
    {
        return $this->presentation->getClientPresentationList($client_id);
    }

    public function createPresentation(Request $request)
    {
        return $this->presentation->createPresentation($request->all());
    }

    public function deletePresentation($id)
    {
        return $this->presentation->deletePresentation($id);
    }

    public function getPersonProfileData($id, $presentation_id) {
        $userId = Auth::user()->id;
        
        try {
            $presentation_profile = PresentationProfile::firstOrCreate(
                ['client_id' =>  $id, 'user_id' => $userId, 'presentations_id' => $presentation_id]
            );
            
            $response = array(
                $this->common->success => true, 
                'data'=> json_decode($presentation_profile->presentation_profile_data), 
                'message' => 'Presentation added / updated successfully.');

        } catch (\Exception $e) {
            $response = array(
                $this->common->success => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            );
        }

        return Response::json($response);

    }

    public function storePresentationProfile(Request $request){
        $data = $request->all();

        $userId = Auth::user()->id;
        $clientId = $data['clientId'];
        $presentationId = $data['presentationId'];

        try {
            $presentation_profile = PresentationProfile::firstOrCreate(
                ['client_id' =>  $clientId, 'user_id' => $userId, 'presentations_id' => $presentationId]
            );
            $presentation_profile->presentation_profile_data = json_encode($data);
            $presentation_profile->save();
            
            $response = array($this->common->success => true, 'message' => 'Presentation added / updated successfully.', 'data' => $data);

        } catch (\Exception $e) {
            $response = array(
                $this->common->success => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            );
        }

        return Response::json($response);
    }

    public function storePresentationPyramid(Request $request) {
        $data = $request->all();

        $userId = Auth::user()->id;
        $clientId = $data['clientId'];
        $presentationId = $data['presentationId'];

        try {
            $pyramid_outcome = PyramidOutcome::firstOrCreate(
                ['client_id' =>  $clientId, 'user_id' => $userId, 'presentations_id' => $presentationId]
            );
            $pyramid_outcome->key_insight = json_encode($data);
            $pyramid_outcome->save();
            
            $response = array($this->common->success => true, 'message' => 'Pyramid data added / updated successfully.');

        } catch (\Exception $e) {
            $response = array(
                $this->common->success => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            );
        }

        return Response::json($response);
    }

    public function getPersonPyramidData($id, $presentation_id) {
        $userId = Auth::user()->id;
        
        try {
            $pyramid_outcome = PyramidOutcome::firstOrCreate(
                ['client_id' =>  $id, 'user_id' => $userId, 'presentations_id' => $presentation_id]
            );
            
            $response = array(
                $this->common->success => true, 
                'data'=> json_decode($pyramid_outcome->key_insight), 
                'message' => 'Pyramid added / updated successfully.');

        } catch (\Exception $e) {
            $response = array(
                $this->common->success => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            );
        }

        return Response::json($response); 
    }

    public function getTransitions($id, $presentation_id) {
        $userId = Auth::user()->id;

        try {
            $story_board = StoryboardSequence::firstOrCreate(
                ['client_id' =>  $id, 'user_id' => $userId, 'presentations_id' => $presentation_id]
            );
            
            $response = array(
                $this->common->success => true, 
                'data'=> json_decode($story_board->story_board_sequence), 
                'message' => 'Transition get successfully.');

        } catch (\Exception $e) {
            $response = array(
                $this->common->success => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            );
        }

        return Response::json($response);
    }

    public function storeTransitions(Request $request) {
        $data = $request->all();

        $userId = Auth::user()->id;
        $clientId = $data['clientId'];
        $presentationId = $data['presentationId'];

        try {
            $story_board = StoryboardSequence::firstOrCreate(
                ['client_id' =>  $clientId, 'user_id' => $userId, 'presentations_id' => $presentationId]
            );
            $story_board->story_board_sequence = json_encode($data);
            $story_board->save();
            
            $response = array($this->common->success => true, 'message' => 'StoryBoard data added / updated successfully.');

        } catch (\Exception $e) {
            $response = array(
                $this->common->success => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            );
        }

        return Response::json($response);
    }

    public function storeOpeningClosing(Request $request) {
        $data = $request->all();

        $userId = Auth::user()->id;
        $clientId = $data['clientId'];
        $presentationId = $data['presentationId'];

        try {
            $opening_closing = OpeningClosing::firstOrCreate(
                ['client_id' =>  $clientId, 'user_id' => $userId, 'presentations_id' => $presentationId]
            );
            $opening_closing->open_close = json_encode($data);
            $opening_closing->save();
            
            $response = array($this->common->success => true, 'message' => 'Opening Closing data added / updated successfully.');

        } catch (\Exception $e) {
            $response = array(
                $this->common->success => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            );
        }

        return Response::json($response);
    }

    public function getOpeningClosing($id, $presentation_id){
        $userId = Auth::user()->id;

        try {
            $opening_closing = OpeningClosing::firstOrCreate(
                ['client_id' =>  $id, 'user_id' => $userId, 'presentations_id' => $presentation_id]
            );
            
            $response = array(
                $this->common->success => true, 
                'data'=> json_decode($opening_closing->open_close), 
                'message' => 'Transition get successfully.');

        } catch (\Exception $e) {
            $response = array(
                $this->common->success => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            );
        }

        return Response::json($response);
    }

}
