<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->insert([
            ['role_name' => 'Admin'],
            ['role_name' => 'Vault user'],
            ['role_name' => 'User']
        ]);
    }
}
