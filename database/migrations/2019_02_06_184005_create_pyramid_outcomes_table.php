<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePyramidOutcomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pyramid_outcomes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id'); // briefcases
            $table->integer('user_id'); // user
            $table->integer('presentations_id');
            $table->json('key_insight')->nullable();
            $table->json('insight')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pyramid_outcomes');
    }
}
