<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresentationProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presentation_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id'); // briefcases
            $table->integer('user_id'); // user
            $table->integer('presentations_id');
            $table->json('presentation_profile_data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presentation_profiles');
    }
}
