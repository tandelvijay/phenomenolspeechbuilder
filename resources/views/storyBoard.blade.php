@extends('layouts.app')
@section('title','StoryBoard Sequence')

@section('content')
<div id="app">
    <div class="container">
        <storyboard-component 
            v-bind:client-id="{{ $id }}" 
            v-bind:pyramid-outcome="{{ !empty($pyramid_outcome) ? $pyramid_outcome : 'No data' }}"
            v-bind:presentation-obj="{{ !empty($presentation) ? $presentation : 'No data' }}"
        > </storyboard-component>
    </div>
</div>
@endsection



