@extends('layouts.app')
@section('title','Presentation-profile')

@section('content')
<div id="app">
    <div class="container">
        <presentation-profile v-bind:client-id="{{ $id }}" v-bind:presentation="{{ $presentation }}"> </presentation-profile>
    </div>
</div>
@endsection



