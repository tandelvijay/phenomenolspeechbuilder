@extends('layouts.app')
@section('title','User Profile')

@section('content')
<div id="app">
    <div class="container">
        <user-profile v-bind:user-profile="{{ $user }}"> </user-profile>
    </div>
</div>
@endsection