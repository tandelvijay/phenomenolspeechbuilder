@extends('layouts.app')
@section('title','Pyramid')

@section('content')
<div id="app">
    <div class="container">
    <div id="printHtml">

        <div class="row mt-5">
            <div class="col-sm-8">
                <hr>
                <table class="border-0 w-100">
                    <tr>
                        <td><h5>Name</h5></td>
                        <td><h5>Date</h5></td>
                        <td><h5>Audience</h5></td>
                    </tr>
                    <tr class="textGrey">
                        <td>{{ $user->name }}</td>
                        <td>{{ $presentation_profile->created_at }}</td>
                        <td>{{ json_decode($presentation_profile->presentation_profile_data)->presentationProfile->audience }}</td>
                    </tr>
                </table>
                <hr>
            </div>
            <div class="col-sm-4 m-auto">
                <div class="actionContent">
                    <h4>Action</h4>
                    <p class="textGrey">{{ json_decode($presentation_profile->presentation_profile_data)->audienceDefination->specific_action }}</p>
                </div>
            </div>
        </div>

        <div class="leadershipMain titleColor">
            <h4>{{ $client->title }}</h4>
            <div class="problemAnalysis bgLightBlue p-3">
                <h5>OPENING AND PROBLEM ANALYSIS</h5>
                <div class="problem">
                    <h6 class="mb-0">Problem:</h6>
                    <div class="textGrey"> {{ json_decode($presentation_profile->presentation_profile_data)->audienceProblem->audience_problem }} </div>
                </div>
                <div class="problem mt-2">
                    <h6 class="mb-0">It has these important manifestations:</h6>
                    <div class="textGrey">
                        <ul>
                            @foreach(json_decode($presentation_profile->presentation_profile_data)->manifestations as $index => $manifestation)
                                <li>
                                    {{ $manifestation->understand }} ({{ $manifestation->body_question }})
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="problem">
                    <h6 class="mb-0">Allusion to the Solution:</h6>
                    <div class="textGrey"> {{ json_decode($opening_closing->open_close)->allude_solution }} </div>
                </div>
                <div>
                    <h5>
                        TRANSITION
                    </h5>
                    <div class="textGrey"> {{ json_decode($story_board->story_board_sequence)->firstInsight }} </div>
                </div>
                
            </div><!-- EO PROBLEM ANALYTICS-->
            <div class="arrow-down"></div>


            <div class="mt-3">
                <div class="row insightsRow">
                    {{ $i = 0 }}
                    @foreach(json_decode($pyramid_outcome)->keyInsigts as $keyInsight)
                        <div class="col">
                            <div class="bgLightBlue p-3">
                                <h5><strong><span class="text-uppercase">{{ $i + 1 }}</span> INSIGHT </strong></h5>
                                <p class="textGrey">
                                    {{ $keyInsight->key }}    
                                </p>
                                <span>Data:</span>
                                <ul>
                                    @foreach($keyInsight->dataPoints as $dataPoint)
                                        <li>
                                            {{ $dataPoint->point }} ({{ $dataPoint->title }})
                                        </li>
                                    @endforeach
                                </ul>
                                <div>
                                    <h6> <strong>TRANSITION</strong> </h6>
                                    @if(($i + 1) < count(json_decode($story_board->story_board_sequence)->transitions))
                                        <p> {{ json_decode($story_board->story_board_sequence)->transitions[$i + 1] }} </p>
                                    @endif
                                    @if(($i + 1) == count(json_decode($story_board->story_board_sequence)->transitions))
                                        <p> {{ json_decode($story_board->story_board_sequence)->closingInsight }} </p>
                                    @endif
                                    
                                </div>
                            </div>
                        </div>
                        {{ $i++ }}
                    @endforeach
                </div>

                <div class="bgLightBlue mt-4">
                    <div class="p-4">
                        <h5>
                            <strong>Closing</strong> 
                        </h5>
                        <p class="textGrey">{{ json_decode($opening_closing->open_close)->closing_tag }}</p>
                    </div>
                </div>

            </div><!-- EO INSIGHT MAIN-->
        </div>

        </div>
    </div>
</div>
@endsection



