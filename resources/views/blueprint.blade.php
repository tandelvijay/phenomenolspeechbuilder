@extends('layouts.app')
@section('title','Pyramid')

@section('content')
<div id="app">
    <div class="container">
        <blueprint-component 
            v-bind:client-id="{{ $id }}" 
            v-bind:pyramid-outcome="{{ !empty($pyramid_outcome) ? $pyramid_outcome : 'No data' }}"
            v-bind:presentation-profile="{{ !empty($presentation_profile) ? $presentation_profile : 'No data' }}"
            v-bind:story-board="{{ !empty($story_board) ? $story_board : 'No data' }}"
            v-bind:opening-closing="{{ !empty($opening_closing) ? $opening_closing : 'No data' }}"
            v-bind:user="{{ !empty($user) ? $user : 'No data' }}"
            v-bind:client="{{ !empty($client) ? $client : 'No data' }}"
        > </blueprint-component>
    </div>
</div>
@endsection



