@extends('layouts.app')
@section('title','Pyramid')

@section('content')
<div id="app">
    <div class="container">
        <opening-component 
            v-bind:client-id="{{ $id }}" 
            v-bind:pyramid-outcome="{{ !empty($pyramid_outcome) ? $pyramid_outcome : 'No data' }}"
            v-bind:presentation-profile="{{ !empty($presentation_profile) ? $presentation_profile : 'No data' }}"
            v-bind:story-board="{{ !empty($story_board) ? $story_board : 'No data' }}"
            v-bind:presentation-obj="{{ !empty($presentation) ? $presentation : 'No data' }}"
        > </opening-component>
    </div>
</div>
@endsection



