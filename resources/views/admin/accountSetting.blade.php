
@extends('layouts.app')
@section('title','Account Setting')

@section('content')
<div id="app">
    <div class="container">
        <account-settings v-bind:user-profile="{{ $user }}"> </account-settings>
    </div>
</div>
@endsection