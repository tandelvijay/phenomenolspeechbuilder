@extends('layouts.app')
@section('title','Manage Users')

@section('content')
<div id="app">
    <div class="container">
        <manage-users v-bind:users="{{ $users }}" v-bind:user-roles="{{ $user_roles }}"></manage-users>
    </div>
</div>
@endsection