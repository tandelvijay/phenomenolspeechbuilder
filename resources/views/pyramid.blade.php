@extends('layouts.app')
@section('title','Pyramid')

@section('content')
<div id="app">
    <div class="container">
        <pyramid-component 
            v-bind:client-id="{{ $id }}" 
            v-bind:presentation-data="{{ !empty($presentation_profile) ? $presentation_profile: 'No data'  }}"
            v-bind:presentation-obj="{{ !empty($presentation) ? $presentation : 'No data' }}"
        > </pyramid-component>
    </div>
</div>
@endsection



