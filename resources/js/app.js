require('./bootstrap');
import Vue from 'vue'
import VueRouter from 'vue-router'

import axios from 'axios'
import VueAxios from 'vue-axios'
import VueHtmlToPaper from 'vue-html-to-paper';

const options = {
    name: '_blank',
    specs: [
    //   'fullscreen=yes',
      'titlebar=yes',
      'scrollbars=yes'
    ],
    styles: [
      '/js/app.js',
      '/css/app.css',
      '/css/style.css'
    ]
  }

Vue.use(VueAxios, axios)

Vue.use(VueRouter);
Vue.use(VueHtmlToPaper, options);

window.Vue = require('vue');
window.VueAxios = require('vue-axios');

window.Axios = require('axios');
window.VueHtmlToPaper = require('vue-html-to-paper');
/**
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
//Vue.component('example-component', require('./components/ExampleComponent.vue').default);


Vue.component('home-component', require('./components/HomeComponent.vue').default);
Vue.component('loading', require('./components/shared/Loading.vue').default);
Vue.component('presentation-component', require('./components/PresentationComponent.vue').default);
Vue.component('presentation-profile', require('./components/PresentationProfileComponent.vue').default);
Vue.component('pyramid-component', require('./components/PyramidComponent.vue').default);
Vue.component('storyboard-component', require('./components/StoryBoardComponent.vue').default);
Vue.component('opening-component', require('./components/OpeningComponent.vue').default);
Vue.component('blueprint-component', require('./components/BlueprintComponent.vue').default);
Vue.component('user-profile', require('./components/UserProfile.vue').default);
Vue.component('account-settings', require('./components/admin/AccountSetting.vue').default);
Vue.component('manage-users', require('./components/admin/ManageUser.vue').default);

const app = new Vue({
    el: '#app'
});
