<?php



//Route::get('/', function () {
//    return view('home');
//});

Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');

Auth::routes();
Route::get('/home/{name}',function(){
    return redirect('/');
})->where('name','[A-Za-z]+'); //Where means(key , value
//)

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::resource('home','ClientController');
    Route::get('/home', 'ClientController@index')->name('home');
    Route::get('getClients', 'ClientController@getClients');
    Route::post('create-client', 'ClientController@createClient');
    Route::delete('delete-Client/{id}', 'ClientController@deleteClient');

    # Presentations
    Route::get('getUserPresentations', 'PresentationController@getUserPresentations');
    Route::get('client-presentations/{client_id}', 'PresentationController@getClientPresentationsView');
    Route::get('getClientPresentations/{client_id}', 'PresentationController@getClientPresentations');
    Route::post('create-presentation', 'PresentationController@createPresentation');
    Route::delete('delete-presentation/{id}', 'PresentationController@deletePresentation');
    //Route::get('client-presentations/profile', 'PresentationController@profilePresentationView');
    Route::get('client-presentations/{id}/{presentation_id}/profile', 'PresentationController@profilePresentationView');
    Route::get('client-presentations/{id}/{presentation_id}/pyramid', 'PresentationController@pyramidView');
    Route::get('client-presentations/{id}/{presentation_id}/storyboard', 'PresentationController@storyboardView');
    Route::get('client-presentations/{id}/{presentation_id}/opening', 'PresentationController@openingView');
    Route::get('client-presentations/{id}/{presentation_id}/blueprint', 'PresentationController@blueprintView');
    Route::post('client-presentations/profile', 'PresentationController@storePresentationProfile');
    Route::get('get-person-profile-data/{id}/{presentation_id}/get', 'PresentationController@getPersonProfileData');
    Route::post('client-presentations/pyramid', 'PresentationController@storePresentationPyramid');
    Route::get('get-person-pyramid-data/{id}/{presentation_id}/get', 'PresentationController@getPersonPyramidData');
    Route::get('get-transitions/{id}/{presentation_id}/get', 'PresentationController@getTransitions');
    Route::post('client-presentations/transitions', 'PresentationController@storeTransitions');
    Route::post('client-presentations/opening-closing', 'PresentationController@storeOpeningClosing');
    Route::get('get-opening-closing/{id}/{presentation_id}/get', 'PresentationController@getOpeningClosing');
    Route::get('get-presentation-pdf/{id}/{presentation_id}/get', 'PresentationController@exportPdf');
    
    Route::get('user-profile', 'UserController@Index')->name('user-profile');
    Route::post('update-user', 'UserController@store')->name('update-user');

    Route::get('account-setting', 'UserController@accountSetting')->name('account-setting');
    Route::get('manage-users', 'UserController@manageUsers')->name('manage-users');
    Route::post('update-users/{id}', 'UserController@update')->name('update-users');
    Route::post('add-users/{id}', 'UserController@addUser')->name('add-users');
    Route::delete('delete-users/{id}', 'UserController@deleteUser')->name('delete-users');
});

